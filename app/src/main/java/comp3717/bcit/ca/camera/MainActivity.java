package comp3717.bcit.ca.camera;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.hardware.Camera.CameraInfo;


public class MainActivity
    extends AppCompatActivity
{
    private static final String TAG = MainActivity.class.getName();
    Preview preview;
    private Camera camera;
    private int cameraId;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        final Camera.PictureCallback jpegCallback;

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        preview = new Preview(this, (SurfaceView)findViewById(R.id.surfaceView));
        preview.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT));
        ((FrameLayout) findViewById(R.id.activity_main)).addView(preview);
        preview.setKeepScreenOn(true);

        jpegCallback = new Camera.PictureCallback()
        {
            public void onPictureTaken(final byte[] data,
                                       final Camera camera)
            {
                new SaveImageTask().execute(data);
                resetCam();
                Log.d(TAG, "onPictureTaken - jpeg");
            }
        };

        preview.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View view)
            {
                if(camera != null)
                {
                    camera.takePicture(null,
                                       // ShutterCallback shutter
                                       null,
                                       // PictureCallback raw,
                                       null,
                                       // PictureCallback postview,
                                       jpegCallback); // PictureCallback jpeg);
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        getCamera(CameraInfo.CAMERA_FACING_BACK);

        if(camera != null)
        {
            camera.startPreview();
            preview.setCamera(camera);
        }
    }

    @Override
    protected void onPause()
    {
        if(camera != null)
        {
            camera.stopPreview();
            preview.setCamera(null);
            camera.release();
            camera = null;
        }

        super.onPause();
    }

    private void getCamera(final int desiredCamera)
    {
        if(!getPackageManager()
            .hasSystemFeature(PackageManager.FEATURE_CAMERA))
        {
            Toast.makeText(this, "No camera on this device", Toast.LENGTH_LONG)
                .show();
        }
        else
        {
            cameraId = findCamera(desiredCamera);

            if(cameraId < 0)
            {
                Toast.makeText(this, "Camera no found.",
                               Toast.LENGTH_LONG).show();
            }
            else
            {
                Log.d(TAG, Integer.toString(cameraId));
                camera = Camera.open(cameraId);
                camera.setDisplayOrientation(90);
            }
        }
    }

    private int findCamera(final int desiredCamera)
    {
        final int numberOfCameras;
        int       cameraId;

        numberOfCameras = Camera.getNumberOfCameras();
        cameraId        = -1;

        for(int i = 0; i < numberOfCameras; i++)
        {
            final CameraInfo info;

            info = new CameraInfo();
            Camera.getCameraInfo(i, info);

            if(info.facing == desiredCamera)
            {
                Log.d(TAG, "Camera found");
                cameraId = i;
                break;
            }
        }

        return (cameraId);
    }

    private void resetCam()
    {
        camera.startPreview();
        preview.setCamera(camera);
    }

    private void refreshGallery(final File file)
    {
        final Intent mediaScanIntent;

        mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(Uri.fromFile(file));
        sendBroadcast(mediaScanIntent);
    }

    private class SaveImageTask
        extends AsyncTask<byte[], Void, Void>
    {
        @Override
        protected Void doInBackground(final byte[]... data)
        {
            FileOutputStream outStream;
            final File       sdCard;
            final File       dir;
            final String     fileName;
            final File       outFile;

            sdCard = Environment.getExternalStorageDirectory();
            dir    = new File (sdCard.getAbsolutePath() + "/camtest");
            dir.mkdirs();

            fileName  = String.format("%d.jpg", System.currentTimeMillis());
            outFile   = new File(dir, fileName);
            outStream = null;

            try
            {
                outStream = new FileOutputStream(outFile);
                outStream.write(data[0]);
                outStream.flush();

                Log.d(TAG, "onPictureTaken - wrote bytes: " + data.length + " to " + outFile.getAbsolutePath());
                refreshGallery(outFile);
            }
            catch(final FileNotFoundException ex)
            {
                Log.e(TAG, "File not found", ex);
            }
            catch(final IOException ex)
            {
                Log.e(TAG, "IOException", ex);
            }
            finally
            {
                try
                {
                    if(outStream != null)
                    {
                        outStream.close();
                    }
                }
                catch(final IOException ex)
                {
                    Log.e(TAG, "IOException", ex);
                }
            }

            return null;
        }

    }
}
